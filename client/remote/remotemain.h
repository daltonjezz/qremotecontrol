#ifndef REMOTEMAIN_H
#define REMOTEMAIN_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QDebug>
#include <QNetworkAddressEntry>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QSound>
#include <QMediaPlayer>

namespace Ui {
class remoteMain;
}

class remoteMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit remoteMain(QWidget *parent = 0);
    ~remoteMain();

private:
    Ui::remoteMain *ui;
    QList<QNetworkAddressEntry> AddressIPv4; //En este objeto almaceno las direcciones IPV4
    QUdpSocket *socketListen;
    QTcpSocket *clientTCP;
    bool statusConnected;
    QHostAddress server;
    quint16 serverPort;

    //el timer sirve para preguntar si el server sigue online.
    QTimer timer;

public slots:
    void readDatagrams();
    void readTCP();
    void disconnectedClient();
    void displayError(QAbstractSocket::SocketError socketError);
    void getIPServer();
private slots:
    void processMessage(QJsonObject mjs);
    void on_pushButton_clicked();
    //void youAreThere();
    void on_bMenos_clicked();
    void on_bShutdown_clicked();
    void on_bMas_clicked();
    void on_bClose_clicked();
    void on_bMute_clicked();
};

#endif // REMOTEMAIN_H
