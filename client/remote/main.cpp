#include "remotemain.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    remoteMain w;
    w.show();

    return a.exec();
}
