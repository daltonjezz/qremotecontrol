#-------------------------------------------------
#
# Project created by QtCreator 2015-12-22T14:05:40
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = remote
TEMPLATE = app


SOURCES += main.cpp\
        remotemain.cpp

HEADERS  += remotemain.h

FORMS    += remotemain.ui

RESOURCES += \
    resourse.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
