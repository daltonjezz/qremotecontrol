#include "remotemain.h"
#include "ui_remotemain.h"

remoteMain::remoteMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::remoteMain)
{
    ui->setupUi(this);
    timer.setInterval(5000);
    timer.start();
    statusConnected=false;

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Obtengo las direcciones ip de la Maquina
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
        QList<QNetworkInterface> allInterfaces = QNetworkInterface::allInterfaces();
        QNetworkInterface eth;

        foreach(eth, allInterfaces) {
            //obtengo todas las entradas de las interfaces
            QList<QNetworkAddressEntry> allEntries = eth.addressEntries();
            QNetworkAddressEntry entry;

            foreach (entry, allEntries) {
                //me quedo solamente con las IPV4 locales.
                if(entry.ip().protocol()==  QAbstractSocket::IPv4Protocol)
                {
                    AddressIPv4.append(entry);
                    qDebug() << "AddressIPv4 : " << AddressIPv4.last().ip().toString() <<"-"<< AddressIPv4.last().broadcast().toString();

                 }

            }
        }
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

 //Primero voy a tener que obtener el IP del servidor, Para esto envio
 // por brodcast consultando la ip.

        clientTCP = new QTcpSocket(this);
        connect(clientTCP,SIGNAL(readyRead()),this,SLOT(readTCP()));
        connect(clientTCP, SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
        connect(clientTCP, SIGNAL(disconnected()), this, SLOT(disconnectedClient()));


        this->socketListen = new QUdpSocket(this);
        this->socketListen->bind(AddressIPv4.last().ip(),5656);
        connect(socketListen, SIGNAL(readyRead()), this, SLOT(readDatagrams()));
        connect(&timer, SIGNAL(timeout()),this,SLOT(getIPServer()));
        getIPServer();
        //---------------------------------------------------------(------------------

}

remoteMain::~remoteMain()
{
    delete ui;
}
void remoteMain::readDatagrams()
{
    if(socketListen->hasPendingDatagrams()) {
           qDebug()<< "el cliente tiene paquetes nuevos";
           QByteArray datagram; //Creo el datragama que voy a leer

           datagram.resize(socketListen->pendingDatagramSize());
           socketListen->readDatagram(datagram.data(), datagram.size(),&this->server, &this->serverPort);
           qDebug()<< datagram.data();
           //Leo el Json.
           QJsonDocument jsonDoc;
           QJsonObject json;
           jsonDoc = jsonDoc.fromJson(datagram,0);
           json = jsonDoc.object(); //convierto lo que recibo a un objeto JSON
           this->processMessage(json);



    }
}

void remoteMain::readTCP()
{
    if(clientTCP->bytesAvailable()>0)
    {
        // get the data
        QByteArray package;
        package = clientTCP->readAll();

        QJsonDocument jsonDoc;
        QJsonObject json;
        jsonDoc = jsonDoc.fromJson(package,0);
        json = jsonDoc.object(); //convierto lo que recibo a un objeto JSON
        //Proceso el mensaje para tomar alguna decision.
        this->processMessage(json);
    }
}

void remoteMain::disconnectedClient()
{
    statusConnected=false;
    this->ui->lIP->setText("not available");
    this->ui->lNivel->setText("[0%]");
    timer.start();

}

void remoteMain::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
        case QAbstractSocket::RemoteHostClosedError:
            break;
        case QAbstractSocket::HostNotFoundError:
            QMessageBox::information(this, tr("Remote Control"),
                                     tr("The host was not found."));
            break;
        case QAbstractSocket::ConnectionRefusedError:
            QMessageBox::information(this, tr("Remote Control"),
                                     tr("The connection was refused by the peer. "
                                        "Make sure the fortune server is running, "));
            break;
        default:
            QMessageBox::information(this, tr("Remote Control"),
                                     tr("The following error occurred: %1.")
                                     .arg(clientTCP->errorString()));
        }
}

void remoteMain::getIPServer()
{
  qDebug() << "Solicito la IP del Server";
    //obtengo los datos del servidor. ip y puerto
    QByteArray datagram;

    //creo un objeto JSON
           QJsonObject json;
           json["codigo"] = 0;
           json["IP"] = "GET ME YOUR IP";

    //Creo el JsonDocument para poder manipularlo
           QJsonDocument jsonDoc(json);
           datagram = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo

    //para esto escribo en el puerto 5657 en brodcast.
    this->socketListen->writeDatagram(datagram,AddressIPv4.last().broadcast(),5657);
}


void remoteMain::processMessage(QJsonObject mjs)
{
    switch (mjs["codigo"].toInt())
    {
       case 100:
        {

            timer.stop();
            clientTCP->connectToHost(mjs["IP"].toString(),5656);
            if(clientTCP->waitForConnected(3000)==true)
            {
                this->ui->lIP->setText("Connected");
                statusConnected=true;
                this->on_bMenos_clicked();
            }
            else
                this->ui->lIP->setText("Not Connected");

        break;
        }
        case 311:
             this->ui->lNivel->setText(mjs["text"].toString());
         break;
    default:
        break;

    }
}

void remoteMain::on_pushButton_clicked()
{
    QSound::play(":/images/Images/tick.wav");
    QCoreApplication::quit();
    close();
}

//void remoteMain::youAreThere()
//{
//    //Estas ahi?
//    if(!this->server.isNull())
//    {
//        qDebug() << "Estas ahi?";
//        QByteArray datagram;
//        QJsonObject json;
//        json["codigo"] = 301;
//        json["text"] = "AreYouThere?";

//        QJsonDocument jsonDoc(json);
//        datagram = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo


//        //para esto escribo en el puerto 5656 directo al server.
//        qDebug()<< "Mensaje a Enviar " << datagram.data();
//        this->socketListen->writeDatagram(datagram,this->server, 5656);
//     }

//}

void remoteMain::on_bMenos_clicked()
{
    QSound::play(":/images/Images/tick.wav");

    //bajar volumen
    if(statusConnected==true)
    {
        qDebug() << "Bajar";
        QByteArray package;
        QJsonObject json;
        json["codigo"] = 101;
        json["text"] = "Server Bajalo";

        QJsonDocument jsonDoc(json);
        package = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo


        //para esto escribo en el puerto 5656 directo al server.
        qDebug()<< "Mensaje a Enviar " << package.data();

        //this->socketListen->writeDatagram(datagram,this->server, 5656);
        clientTCP->write(package);
     }
}

void remoteMain::on_bShutdown_clicked()
{
    //Apagar
    QSound::play(":/images/Images/tick.wav");
    if(statusConnected==true)
    {
        qDebug() << "Apagando Server";

        QByteArray package;
        QJsonObject json;
        json["codigo"] = 201;
        json["text"] = "Apagar";

        QJsonDocument jsonDoc(json);
        package = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo


        //para esto escribo en el puerto 5656 directo al server.
        qDebug()<< "Mensaje a Enviar " << package.data();

        //this->socketListen->writeDatagram(datagram,this->server, 5656);
        clientTCP->write(package);
     }
}

void remoteMain::on_bMas_clicked()
{
    QSound::play(":/images/Images/tick.wav");
    //subir volumen
    if(statusConnected==true)
    {
        qDebug() << "Subirlo";
        QByteArray package;
        QJsonObject json;
        json["codigo"] = 102;
        json["text"] = "Server Subilo";

        QJsonDocument jsonDoc(json);
        package = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo


        //para esto escribo en el puerto 5656 directo al server.
        qDebug()<< "Mensaje a Enviar " << package.data();

        //this->socketListen->writeDatagram(datagram,this->server, 5656);
        clientTCP->write(package);
     }
}

void remoteMain::on_bClose_clicked()
{
    close();
}

void remoteMain::on_bMute_clicked()
{
    QSound::play(":/images/Images/tick.wav");
    //subir volumen
    if(statusConnected==true)
    {
        qDebug() << "Subirlo";
        QByteArray package;
        QJsonObject json;
        json["codigo"] = 103;
        json["text"] = "Server Silencio";

        QJsonDocument jsonDoc(json);
        package = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo


        //para esto escribo en el puerto 5656 directo al server.
        qDebug()<< "Mensaje a Enviar " << package.data();

        //this->socketListen->writeDatagram(datagram,this->server, 5656);
        clientTCP->write(package);
     }
}
