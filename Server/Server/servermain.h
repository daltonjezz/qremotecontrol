#ifndef SERVERMAIN_H
#define SERVERMAIN_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QDebug>
#include <QNetworkAddressEntry>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QRegExp>
#include <QTcpSocket>
#include <QTcpServer>

namespace Ui {
class ServerMain;
}

class ServerMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit ServerMain(QWidget *parent = 0);
    ~ServerMain();

private:
    Ui::ServerMain *ui;
    QList<QNetworkAddressEntry> AddressIPv4; //En este objeto almaceno las direcciones IPV4

    QTcpServer *serverTCP;
    QTcpSocket *clientTCP;
    bool statusClient;
    QUdpSocket *socketInfo ;
    QHostAddress sender;
    quint16 senderPort;
    bool Mute;

public slots:
    void readDatagrams();
    void readDatagramsInfo();
    void processMessage(QJsonObject msj);
    void newClient();
    void readTCP();
    void disconnectedClient();

//    void amixerOutput();
//    void amixerError();

private slots:
    void on_pushButton_clicked();
};

#endif // SERVERMAIN_H
