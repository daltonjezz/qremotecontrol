#-------------------------------------------------
#
# Project created by QtCreator 2015-12-22T12:13:04
#
#-------------------------------------------------

QT       += core gui network opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Server
TEMPLATE = app


SOURCES += main.cpp\
        servermain.cpp

HEADERS  += servermain.h \
    message.h

FORMS    += servermain.ui

RESOURCES += \
    resourse.qrc
