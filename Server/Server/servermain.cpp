#include "servermain.h"
#include "ui_servermain.h"

ServerMain::ServerMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ServerMain),statusClient(false),Mute(false)
{
    ui->setupUi(this);
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++
Obtengo las direcciones ip de la Maquina
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    QList<QNetworkInterface> allInterfaces = QNetworkInterface::allInterfaces();
    QNetworkInterface eth;

    foreach(eth, allInterfaces) {
        //obtengo todas las entradas de las interfaces
        QList<QNetworkAddressEntry> allEntries = eth.addressEntries();
        QNetworkAddressEntry entry;

        foreach (entry, allEntries) {
            //me quedo solamente con las IPV4 locales.
            if(entry.ip().protocol()==  QAbstractSocket::IPv4Protocol)
            {
                AddressIPv4.append(entry);
                qDebug() << "AddressIPv4 : " << AddressIPv4.last().ip().toString() <<"-"<< AddressIPv4.last().broadcast().toString();

             }

        }
    }
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Declaro los Sockets
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



    this->serverTCP = new QTcpServer(this);
    //this->QTcpServer->bind(AddressIPv4.last().ip(),5656);

    if(!serverTCP->listen(AddressIPv4.last().ip(), 5656))
       {
           qDebug() << "Server could not start";
       }
       else
       {
           qDebug() << "Server started!";
       }

    this->socketInfo = new QUdpSocket(this);
    this->socketInfo->bind(AddressIPv4.last().broadcast(),5657);

    //seteo los label de informacion
    this->ui->lIP->setText(AddressIPv4.last().ip().toString());
    this->ui->lPort->setText("5656");

    //connect(socketListen, SIGNAL(readyRead()), this, SLOT(readDatagrams()));
    connect(socketInfo, SIGNAL(readyRead()), this, SLOT(readDatagramsInfo()));
    connect(serverTCP, SIGNAL(newConnection()), this, SLOT(newClient()));




}

ServerMain::~ServerMain()
{
    delete ui;
}

void ServerMain::readDatagrams()
{
//    if(socketListen->hasPendingDatagrams()) {
//           qDebug()<< "El server tiene paqutes para esuchar";
//           QByteArray datagram;
//           datagram.resize(socketListen->pendingDatagramSize());
//           socketListen->readDatagram(datagram.data(), datagram.size(),&this->sender, &this->senderPort);


//               qDebug() << datagram.data();
//                //obtengo el JSON para poder hacer las deciciones en base a mi protocolo

//               QJsonDocument jsonDoc;
//               QJsonObject json;
//               jsonDoc = jsonDoc.fromJson(datagram,0);
//               json = jsonDoc.object(); //convierto lo que recibo a un objeto JSON
//               //Proceso el mensaje para tomar alguna decision.
//               this->processMessage(json);

//    }
}

void ServerMain::readDatagramsInfo()
{
    if(socketInfo->hasPendingDatagrams()) {
           qDebug() << "Alguien Solicita Conectarse";
           QByteArray datagram;
           datagram.resize(socketInfo->pendingDatagramSize());
           socketInfo->readDatagram(datagram.data(), datagram.size(),&this->sender, &this->senderPort);

           qDebug() << datagram.data();

    //creo un objeto JSON
           QJsonObject json;
           json["codigo"] = 100;
           json["IP"] = AddressIPv4.last().ip().toString();
           json["PORT"] = 5656;
    //Creo el JsonDocument para poder manipularlo
           QJsonDocument jsonDoc(json);


           datagram.clear();
           datagram = jsonDoc.toJson(); //Convierto el Json a json para enviarlo
           this->senderPort=5656;
           this->socketInfo->writeDatagram(datagram.data(),this->sender,5656);

}

}

void ServerMain::processMessage(QJsonObject msj)
{

    //Procesando MENSAJE

    switch (msj["codigo"].toInt())
   {
       case 101 :
        {
             qDebug() << "Server Baja volumen";

             QProcess amixer;
             amixer.start("amixer", QStringList() << "set" << "Master" << "5%-" );
             amixer.waitForFinished();
             QByteArray output = amixer.readAll();
             qDebug() << output.data();
             amixer.close();
             //Voy a enviar el  nivel de Volumen;
             QStringList strLines = QString(output).split(QRegExp(":"));
             qDebug() << strLines.last().split(" ").at(3);
             QByteArray package;
             QJsonObject json;
             json["codigo"] = 311;
             json["text"] = strLines.last().split(" ").at(3);

             QJsonDocument jsonDoc(json);
             package = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo
             this->clientTCP->write(package);



        break;
        }
       case 102 :
           {
             qDebug() << "Server Subi volumen";

             QProcess amixer;
             amixer.start("amixer", QStringList() << "set" << "Master" << "5%+" );

             amixer.waitForFinished();
             QByteArray output = amixer.readAll();
             amixer.close();
             QStringList strLines = QString(output).split(QRegExp(":"));

             QByteArray package;
             QJsonObject json;
             json["codigo"] = 311;
             json["text"] = strLines.last().split(" ").at(3);

             QJsonDocument jsonDoc(json);
             package = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo
             this->clientTCP->write(package);


            break;
            }
            case 103 :
                {
                  qDebug() << "Server Mute/Unmute";
                  QProcess pactl;
                  if(Mute==false)
                    {
                    pactl.start("pactl", QStringList() <<"set-sink-mute" <<"0"<<"0");
                    Mute=true;
                  }
                  else{
                      pactl.start("pactl", QStringList() <<"set-sink-mute" <<"0"<<"1");
                      Mute=false;
                    }
                  pactl.waitForFinished();

                //---------------------------------------
                  QProcess amixer;
                  amixer.start("amixer", QStringList() <<"get" <<"Master");
                  amixer.waitForFinished();
                  QByteArray output = amixer.readAll();
                  amixer.close();

                  QStringList strLines = QString(output).split(QRegExp(":"));

                  QByteArray package;
                  QJsonObject json;
                  json["codigo"] = 311;
                  json["text"] = strLines.last().split(" ").at(3);

                  QJsonDocument jsonDoc(json);
                  package = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo
                  this->clientTCP->write(package);


                 break;
                 }
       case 201 :
             {
            qDebug() << "Apagamos Todo POR FAVOR";
             QProcess shutdown;
             shutdown.start("shutdown", QStringList() << "-h" << "now");
             shutdown.waitForFinished();
             QByteArray output = shutdown.readAll();

             qDebug() << output.data();
            break;
            }


//       case 301 :
//            qDebug() << "Eiii seguis ahi?";
//            //Respondo el Mensaje
//            if(!this->sender.isNull())
//            {
//                qDebug() << "Bajar";
//                QByteArray datagram;
//                QJsonObject json;
//                json["codigo"] = 311;
//                json["text"] = "yes, i am here.";

//                QJsonDocument jsonDoc(json);
//                datagram = jsonDoc.toJson(); //Convierto el Json a binary para enviarlo


//                //para esto escribo en el puerto 5656 directo al server.
//                qDebug()<< "Mensaje a Enviar " << datagram.data();
//                this->socketListen->writeDatagram(datagram,this->sender, 5656);
//             }


//       break;
   default:
       break;

    }
}

void ServerMain::newClient()
{
   if(statusClient == false)
   {
    //Cuando se conecta un cliente lo declaro
    clientTCP = serverTCP->nextPendingConnection();
    statusClient = true;
    this->ui->lStatus->setText("Connected");
    connect(clientTCP, SIGNAL(disconnected()), clientTCP, SLOT(deleteLater()));
    connect(clientTCP, SIGNAL(readyRead()), this, SLOT(readTCP()));
    connect(clientTCP, SIGNAL(disconnected()), this, SLOT(disconnectedClient()));

   }
}

void ServerMain::readTCP()
{
    if(clientTCP->bytesAvailable()>0)
    {
        // get the data
        QByteArray package;
        package = clientTCP->readAll();

        QJsonDocument jsonDoc;
        QJsonObject json;
        jsonDoc = jsonDoc.fromJson(package,0);
        json = jsonDoc.object(); //convierto lo que recibo a un objeto JSON
        //Proceso el mensaje para tomar alguna decision.
        this->processMessage(json);
    }
}

void ServerMain::disconnectedClient()
{
  this->ui->lStatus->setText("Disconnect");
  statusClient = false;
}

//void ServerMain::amixerOutput()
//{
//    QByteArray byteArray = this->aMixer->readAllStandardOutput();
//    QStringList strLines = QString(byteArray).split("\n");

////        foreach (QString line, strLines){
////            qDebug() << line;
////        }
//}

//void ServerMain::amixerError()
//{
//    QByteArray byteArray = this->aMixer->readAllStandardOutput();
//    QStringList strLines = QString(byteArray).split("\n");

//        foreach (QString line, strLines){
//            qDebug() << line;
//        }
//}

void ServerMain::on_pushButton_clicked()
{
    close();
}
